# Data types

In Day 02, we talked about the different sigils present in Raku,
their associated type constraints, and default types. For example,
positional variables are constrained by `Positional` and their
default type is `Array`.

A **data type** is collection of related values. 
A variable's type is useful information and Raku makes it easy to find
out by invoking `.^name` on the variable:

```raku
my $scalr;
my @posit;
my %assoc;
my &callb;

say $scalr.^name; #=> «Any␤»
say @posit.^name; #=> «Array␤»
say %assoc.^name; #=> «Hash␤»
say &callb.^name; #=> «Callable␤»
```

As discussed in Day 02 and illustrated here, each type of variable has a
default type. For instance, a scalar variable can accept any type
of value by default whereas an associative variable only accepts values
of type `Hash`.

Nonetheless, we can specify the type of values a variable can hold; Raku is a
*gradually*-typed programming language. This means we control which parts of
the code are which by either leaving out types or by adding them in:

```raku
my Int     $age  = 35;     # an integer
my Rat     $fra  = 1/2;    # a fraction
my Num     $pi   = 2e300;  # a floating-point number
my Complex $imag = 1+2i;   # a complex number
my Str     $name = 'John'; # a string
```

To check if a variable is of a certain type, we can smartmatch the variable
against the type.

## Booleans

Raku’s boolean values are represented by type `Bool` which can either be
`True` for true and `False` for false. To check if an argument is boolean,
we can smartmatch agains `Bool`.

```raku
say True ~~ Bool; #=> «True␤»
say "Hi" ~~ Bool; #=> «False␤»
```

The prefix operator `?` coerces an argument to `Bool`. Alternatively,
the routine `so` can be used, although it has a looser precedence than
`?`:

```raku
say ?False;   #=> «False␤»
say False.so; #=> «False␤»
say so "Hi";  #=> «True␤»
say 0.so;     #=> «False␤»
```

The prefix operator `!` coerces an argument to `Bool` and negates the result.
Alternatively, the routine `not` can be used, although it has a looser
precedence than `!`:

```raku
say !False;    #=> «True␤»
say False.not; #=> «True␤»
say not "Hi";  #=> «False␤»
say 0.not;     #=> «True␤»
```

In Raku, the following values evaluates to `False`: `0`, `''` (the empty
string), `Nil`, and `False` itself.

## Numbers

In Raku, numbers are hierachically organized by their widths. From the 
narrowest to the widest:

* An `Int` is an integral number.
* A `Rat` is a number that can be expressed as a fraction (aka, a rational number
  hence the type's name). A `Rat` has *limited-precision* numerator and
  denominator.
* A `FatRat` is a number conceptually conceptually similar to a `Rat`, however
  it has *arbitrary-precision* numerator and denominator.
* A `Num`s is a number cannot be represented as a fraction (aka,
  floating point number).
* A `Complex` is a number that belongs to the plane of complex numbers.

All of these types falls within the broader category `Numeric` which
represents any entity that can act as a number.

```raku
say 42   ~~ Int;     #=> «True␤»
say 42   ~~ Rat;     #=> «False␤»
say 42   ~~ Num;     #=> «False␤»
say 42   ~~ Complex; #=> «False␤»

say 3.14 ~~ Int;     #=> «False␤»
say 3.14 ~~ Rat;     #=> «True␤»
say 3.14 ~~ Num;     #=> «False␤»
say 3.14 ~~ Complex; #=> «False␤»

say 1/2  ~~ Int;     #=> «False␤»
say 1/2  ~~ Rat;     #=> «True␤»
say 1/2  ~~ Num;     #=> «False␤»
say 1/2  ~~ Complex; #=> «False␤»

say 2e30 ~~ Int;     #=> «False␤»
say 2e30 ~~ Rat;     #=> «False␤»
say 2e30 ~~ Num;     #=> «True␤»
say 2e30 ~~ Complex; #=> «False␤»

say 2+i  ~~ Int;     #=> «False␤»
say 2+i  ~~ Rat;     #=> «False␤»
say 2+i  ~~ Num;     #=> «False␤»
say 2+i  ~~ Complex; #=> «True␤»
```

Raku integers need not be specified in decimal (base 10) format. They can be
specified in binary by prefixing the numeral with `0b`. The octal prefix is
`0o` (base 8) and the hexadecimal (base 16) prefix is `0x`. The optional decimal
prefix is `0d`:

```raku
say 0b1111; #=> «15␤»
say 0o17;   #=> «15␤»
say 0xF;    #=> «15␤»
say 0d15;   #=> «15␤»
```

## Str

Strings are sequences of characters, however they are immutable and aren't
indexable. There are several ways to specify strings, but the most common
way is enclosing the constituent characters in either single or double quotes.

```raku
> "Hello, World!"
Hello, World!
> 'Hola, Mundo!'
Hola, Mundo!
```

New strings can be created by appending other strings with the `~` operator:

```raku
> "Raku" ~ " is " ~ "ofun."
Raku is ofun.
```

To check if an entity is a string, smartmatch agains the type `Str`:

```raku
> "Hello" ~~ Str
True
> 2 ~~ Str
False
> '2' ~~ Str
True
```

While strings themselves cannot be individually accessed, Raku provides the
`split` routine which splits a string up into pieces based on delimiters found
in the string and returns a sequence of its characters which can be 
accessed sequentially. The `split` routines also returns empty strings before or
after a delimiter, so we use the `skip-empty` flag to return only the non-empty
characters:

```raku
> "Hello".split('', :skip-empty)[0] # splitting by the empty string
H
> "Hello".split('', skip-empty => True)[1]
e
> "Hello".split('', :skip-empty)[2]
l
```

Strings themselves are immutable, however the variables storing them are not.
For example, trying to do an in-place substitution with the `s///` operator
throws an error regarding the immutability of literal strings:

```raku
> 'jello' ~~ s/j/H/ # replace first instance of 'j' with 'H'
Cannot modify an immutable Str (jello)
  in block <unit> at <unknown file> line 1
```

Placing the string in a variable does the trick:

```raku
> my $greeting = 'jello'
jello
> $greeting ~~ s/j/H/ # replace first instance of 'j' with 'H'
｢j｣
> say $greeting
Hello
```

The `s///` operator is the in-place syntactic variant of the `subst` routine.
Thus, the same effect can be achieved by assigning the substituted string
back to the original string:

```raku
> my $greeting = "jello"
jello
> $greeting = $greeting.subst(/j/, 'H')
Hello
> say $greeting
Hello
```

## List

A list is a collection of items sequentially and potentially lazily. Lists
are 0-indexed and can be created by separating their elements by commas
and semi-colons (for multidimensional lists).

```raku
my $evens = (2, 4, 6, 8);
say $evens[0]; #=> 2
say $evens[1]; #=> 4
```

Lists are immutables (i.e., you cannot change a list's size), however their
items can be containerized and thus mutated.

```raku
my $x = 4;
my $odds = (1, 3, $x);
say $odds; #=> (1 3 4)

$odds.push(7); #=> Cannot call 'push' on an immutable 'List'
$odds.shift;   #=> Cannot call 'shift' on an immutable 'List'

$odds[2] = 5;
say $odds; #=> (1 3 5)
```

### Arrays

Unlike `List`s, `Array`s are immutable data structures themselves and
their items are automatically containerized so they can be also be mutated.
Square brackets can be used to declare an `Array`, but if `@`-sigiled
then this is already implied in which case the parenthesis/square brackets
can be dropped.

```raku
my @odd = 1, 3, 4;
say @odd.pop; #=> 4
@odd.push(5);
```

### Pairs

### Hashes

