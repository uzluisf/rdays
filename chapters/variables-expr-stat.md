# Variables, expressions, and statements

One of the most powerful features of a programming language is the ability to
manipulate **variables**. Broadly speaking, a variable is a container or slot
memory, associated with a name, where you can store data values.

In Raku, most variables are declared with the `my` declarator which declares
a lexical variable, i.e., the variable only exists within the current block. A
variable must be first declared before you use it. Either you can declare a
variable and then assign to it or both declare the variable and assign to it in
a single statement:

```raku
> my $number;       # declaring the variable $number
(Any)
> $number = 42;     # assigning to the variable $number
42
> my $name = 'Jae'; # variable declaration and assignment
Jae
>
```

Neglecting to declare a variable causes the compiler to throw a
compiler-time error (In Raku, compiler-time errors are identified by the
string `"===SORRY!==="`):

```raku
> $number = 2
===SORRY!=== Error while compiling:
Variable '$number' is not declared
------> <BOL>⏏$number = 2
>
```

## Sigils

In Raku, variable names usually start with what's called a *sigil*, i.e.,
a special non-alphanumeric character such as `$`, `@`, `%`, and `&`.
This special character tells to both us and the compiler the kind of
variable we're dealing with. In general, variables can be *scalar*,
*positional*, *associative*, or *callable*.

### Scalar variables

Scalar variabler are variables that can store only a single value at any given
time and they're identified by the scalar-sigil `$`. The type constraint for
them is `Mu` (i.e., no type constraint) and `Any` is their default type.

```raku
my $fruit01 = 'Mango';
my $fruit02 = 'Orange';
my $fruit03 = 'Banana';
my $fruit04 = 'Apple';
my $fruit05;

say $fruit01; #=> «Mango␤»
say $fruit03; #=> «Banana␤»
say $fruit05; #=> «(Any)␤»
```

### Positional variables

Positional variables can store a collection of values that can be accesed 
*positionally* via an *index*; such index is a nonnegative integer number
that indicates the position of a value in the variable. They're represented by
the positional-sigil `@`. The type constraint for them is `Positional`
and their default type is `Array`.

```raku
my @fruits = 'Mango', 'Orange', 'Banana', 'Apple';

say @fruits[0];  #=> «Mango␤»
say @fruits[2];  #=> «Banana␤»
say @fruits[3];  #=> «Apple␤»
say @fruits[10]; #=> «(Any)␤»
```

### Associative variables

Much like positional variables, associative variables store a collection of
items but these values are accesed via a *key*; these items are known as
`Pair`s which consist of two parts, a key and a value. The associative-sigil `%`
represents them. The type constraint for them is `Associative` and their default
type is `Hash`.

```raku
my %en-to-es = one => 'uno', two => 'dos', three => 'tres';

say %en-to-es{'one'};   #=> «uno␤»
say %en-to-es{'two'};   #=> «dos␤»
say %en-to-es{'three'}; #=> «tres␤»
say %en-to-es{'fifty'}; #=> «(Any)␤»
```

### Callable variables

Callable variables can store values that can be called or invoked such as
blocks, functions, etc. The callable-sigil `&` represents them. Both the type
constraint and the default type for callable variables is `Callable`.

```raku
my &say-hello = { say "Hello" };
my &world = sub { say "World" };

hello(); #=> «Hello␤»
world(); #=> «World␤»
```

### Why sigils?

There are several reasons for the presence of sigils in Raku.

* They make variable interpolation into strings quite easy.

* They form micro-namespaces for different variables, and thus avoid name
  clashes. For example, both `$books` and `@books` are two different variables.
  The former is scalar variable and the latter a positional one.

* They allow easy single/plural distinction. In programming languages without
  this distinction, you must often rely on the variable's name provided that the
  programmer honors the convention of distinguishing betweem single values and
  collection of values. 

* They work much like natural languages that use mandatory noun markers, so our
  brains are built to handle it.

* They aren't mandatory, since you can declare sigilless names.

## Expressions and statements

An **expression** is a combination of terms and operators. A term can be a
variable or a literal, i.e., data that's represented directly within the program
such as a number or a string. A value all by itself is considered an expression,
ans so is a variable so all the following are valid expressions:

```raku
> 5
5
> "five"
five
> my $pi = 3.14
3.14
> $pi + 5
8.14
```

A **statement** is a unit of code that has effect, such as creating a variable,
or displaying a value. In Raku, statements needs to end with a semi-colon (`;`),
however the semi-colon can sometimes be omitted:

```raku
> say 42; say "Hello"
42
Hello
```




