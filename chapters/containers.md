# Containers

In programming languages, a variable is a way of associating a particular
value (an object) with a name known to the compiler. For instance, take
the variable declaration `my $x = "Hello"`. Normally, you'd think the string
`"Hello"` as simply being stored in `$x`. However, this isn't the entire story
when it comes to Raku since it hides the existence of *containers*.

In Raku, when the compiler encounters the variable declaration `my $x`,
the compiler registers it in some internal symbol table. A symbol table generally 
provides object lookup by name and allows the compiler to get an object
from the mention of its name in a program's source code. In the case of `my $x =
"Hello"`, the lexical pad entry for the variable `$x` is a pointer to an object
of type `Scalar` that serve as the *container* for the object `"Hello"`.
Pictorially, this could be represented as follows:

```text
 Table      Container       Value
-------       +---+      +---------+
$x | *   ---> | * | ---> | "Hello" |
-------       +---+      +---------+
```

In the diagram, the cell (labeled `*`) in the symbol table is associated with
the name `$x`. This is known as *binding*. Names are often bound to objects that
serve as containers for other objects (e.g., `Scalar` as the container for the
object `"Hello"`), but that isn't necessarily always the case as we'll see in
the next section. Names using the sigil `$` make use of the `Scalar` container
and can be bound to any object.

## Item container

In Raku, a variable is usually prefixed with a `$` sigil which indicates
the variable  and is given a value with a binding operator (`:=`):

```
my $y := 42;
```

Pictorially:

```text
 Table           Value
-------        +---------+
$y | *   --->  | "Hello" |
-------        +---------+
```

As you might've realized, this is one of the cases in which a name (e.g., `$y`)
isn't bound to an object that serves as a container for another object. Instead,
the name is bound directly to that object (e.g., `42`). In short, we've bounded
the value `42` to the name `$name`, which is the purpose of the binding operator.
Another way of interpreting it is that both `$x` and `42` now represents the
literal value `42`. This implies that after a name has been bound to a value,
you cannot rebind it:

```raku
42 := 45; #= Cannot use bind operator with this left-hand side
$x := 45; #= Cannot use bind operator with this left-hand side
```

This is because the object holding `42` will never change to represent another
different object. In Raku, *value types* (and their *instance objects*)
such as `Int` (e.g., `42`) and `Str` (e.g., `"Hello"`) are immutable.

How do we mutate a variable after a value has been bound to it? For starters,
we can try getting hold of `Scalar` container that will serve as proxy
between the variable's name and the value. We can't instantiate a `Scalar` via
the `.new` method, but the sigil `$` is perfect for this task. We declare 
a nameless lexical variable and bind it to the variable:

```raku
my $x := my $;
say $x;   #=> (Any)
$x := 42; # Cannot use bind operator with this left-hand side
```

However, this isn't of much help as evidenced by the error.
When we declared the variable `$x`, the value of `my $` was bound to it
and trying to rebind the variable will cause the same error as before.
Hopefully this is clear by now but the reason is that we're trying to rebind
to a value and thus replace it altogether; we cannot do that. However,
we're not far off the mark; what we want is to be able to replace a variable's
value after it's been declared but without replacing the container itself.
This is where the assignment operator (`=`) operator comes in, as a close
cousin of the binding operator (`:=`).

Unlike the binding operator which replaces the container itself, the assignment
operator places the value of the right-hand side into the container on the
left-hand side of the assignment.

Let's try again. This will be done in a single statement but what we're doing
is placing a value in a container (nameley, `Scalar`), and then binding 
said container to a variable:

```raku
my $x := my $ = 42;
say $x; #=> 42
$x = 45;
say $x; #=> 45
```
As illustrated above, we can now assign an original value right from the start,
as well as replace it with other value. We can even add a type constraint to
the container:

```raku
my $x := my Int $ = 42;
$x = 1;     # OK!
$x = 'One'; # Error: Type check failed in assignment;
            # expected Int but got Str ("One")
```

`Scalar` containers delegate all method calls to the value which they contain;
whenever you use a container it uses the value it contains, and for the
most part it's pretty much invisible. Thus, containers keep out of the way
and stay hidden. For instance, given:

```raku
my $nvalue := "hello";
my $cvalue := my Int $ = "hello";

say $nvalue; #=> hello
say $cvalue; #=> hello

say $nvalue.substr(0, 2); #=> he
say $cvalue.substr(0, 2); #=> he
```

you'd have a hard time telling the difference between `$value` and
`$cont-value`. Unless you draw upon the features that containers provide
such as assigning to variables, passing *rw* objects to a function, etc,
you won't notice them. In order to check whether a variable is bound either to
a value or a container, we can use the `VAR` method which returns the underlying
`Scalar` object, if any. We might also need to use the method in conjunction
with `.^name`:

```raku
my $nvalue := "hello";
my $cvalue := my Int $ = "hello";

say $nvalue.VAR; #=> Str
say $cvalue.VAR; #=> Str

say $nvalue.VAR.^name; #=> Str
say $cvalue.VAR.^name; #=> Scalar
```

So far, we've been able to bind both a single value and a container
to a variable. How about binding a list of values to a variable? In Raku, we
declare a literal list (`List`) of values using commas and/or semicolons:

```raku
my $evens := 2, 4, 8;
say $evens;              #=> (2 4 8)
say $evens.VAR;          #=> (2 4 8)
say $evens.VAR.^name;    #=> List

say $evens[0];           #=> 2
say $evens[0].VAR;       #=> Int
say $evens[0].VAR.^name; #=> Int

$even := 3;     # Cannot use bind operator with this left-hand side
$evens[2] := 6; # Cannot use bind operator with this left-hand side 
```

As you can see, attempting to bind some value gets us an error, regardless of 
whether the values comes from a `List` or a literal value. This is how
`List`s manage to be immutable in Raku; neither the list itself nor
its values can be mutated.

In order to create a list list whose elements can be mutated, we can replicate
the process we went through before with a single value: containerize 
each of the list's slots (or at least, those that we need to be mutable),
and bind the whole list to the variable. Thus, instead of a list of literal
values, we end up with a list of `Scalar` objects:

```raku
my $evens := my $ = 2, my $ = 4, my $ = 8;
say $evens;              #=> (2 4 8)
say $evens.VAR.^name;    #=> List
say $evens[0].VAR.^name; #=> Scalar
say $evens[0];           #=> 2
$evens[2] = 6;
```

We've containerized each of the list's items, not the list itself. Therefore,
while we can mutate each of its elements we cannot assign to the `$evens`
variable. To do so we can just containerize the list itself:

```raku
my $evens := my $ = (my $ = 2, my $ = 4, my $ = 8);
say $evens.VAR.^name;    #=> Scalar
say $evens[0].VAR.^name; #=> Scalar

$evens = (2, 4, 6);      # OK!
```

Each of the `Scalar` objects can also be type contrained:

```raku
my $evens := my $ = (my Int $ = 2, my Int $ = 4, my Int $ = 8);
$evens[2] = 6;      # OK!
$evens[1] = 'four'; # Typecheck failure
```

So far we've only added type constrains to containers holding values
such as `2` and `"Hi"`, however we can do the same for `List`. For a variable
like `$evens`, we might need it only to hold a list of things which are
accessed by their indexes. In Raku, there's the `Positional` role for this task.
This is the same role implemented by types such as `List`, `Array`, etc. that
support indexing using the operator `[]`. Thus, we can constrain the container
for the list so the variable `$evens` can only hold `Positional` types
like `List`. 

```raku
# This works
my $evens := my Positional $ = (my Int $ = 2, my Int $ = 4, my Int $ = 8);

# This doesn't work
my $odds := 3; # Type check failed in binding;
               # expected Positional but got Int (2)
```

For the sake of continuity, we've constrained the container directly
but we can just constrain the variable which is the same as contraining
the container that is eventually bound to it. In fact, this is what youl'll be
doing anyway. The variable `$evens` ends up being a `Positional` variable whose
value is a list with mutable items. It bears repeating that the list itself is
immutable and only its elements are mutable. For example, you cannot neither add
nor remove elements from the list.

## Compound containers

Declaring a `Positional` variable whose value is a list with mutable items
as done in the previous section looks awfully verbose. Thankfully, Raku provides
some syntax to simplify it.

First, we don't need to explicitly type constrain the variable with
`Positional`. Instead of the sigil `$`, we can use the sigil `@` to indicate
that the variable has a `Positional` type constraint:

```raku
my Positional @evens := 2  ; # Type check failed in binding;
                             # expected Positional but got Int (2)

my Positional @odds  := 1, ; # a single-element list,
                             # notice the trailing comma
```

Second, we'll surround our comma-separated list with square brackets. This tells
the compiler to create an `Array` instead of a `List`. Unlike a `List`, 
an `Array` is itself mutable and each of its elements are mutable because
they're all put into `Scalar` containers automatically, just like we did 
manually in the previous section. By the way, we can surround our lists with
parentheses but there are only needed where precedence is an issue; in Raku
commas, not parentheses, create lists.

```raku
my @evens := [2, 4, 8];

say @evens.VAR;          #=> [2 4 8]
say @evens.VAR.^name;    #=> Array
say @evens[0].VAR.^name; #=> Scalar

@evens[2] = @evens[2] - 2;
@evens[3] = @evens[2] + 2;
@evens[4] = @evens[2] + 2;

say @evens; #=> [2 4 6 8 10]
```

Our code became a lot shorter, but we can toss out a couple more
characters. As demonstrated previously, assigning to a `$`-sigiled variable
gives you a `Scalar` container for free. This is also the case while assigning
to `@`-sigiled variable; it provides you with an `Array` container for free.
If we switch to assignment, our previous code can become a lot shorter by
dropping the square brackets altogether. We know they instruct the compiler to
create an `Array` but this is already done by `@` when performing an assigment:

```raku
my @evens = 2, 4, 8;

say @evens.VAR;          #=> [2 4 8]
say @evens.VAR.^name;    #=> Array
say @evens[0].VAR.^name; #=> Scalar
```

Everything we've done here can be applied to `%`-sigiled and
`&`-sigiled variables:

* The `%` sigil variable implies an `Associative` type constraint for
  name-based lookup via the operator `{}`, and it offers the same shortcuts for
  assignment which gives you a `Hash` and creates `Scalar` containers for each
  of the values:
  
  ```raku
  my $l2n := (A => 1, B => 2, C => 3);
  say $l2n;       #=> (A => 1 B => 2 C => 3)
  say $l2n[0]<A>; #=> 1
  
  # using %() to get a Hash container is akin to
  # using [] to get an Array container.
  my $l2n := %(A => 1, B => 2, C => 3);
  say $l2n;    #=> {A => 1, B => 2, C => 3}
  say $l2n<A>; #=> 1
  $l2n<A> = 0;
  
  my %l2n = A => 1, B => 2, C => 3;
  say %l2n;    #=> {A => 1, B => 2, C => 3}
  say %l2n<A>; #=> 1
  %l2n<A> = 0;
  ```

* The `&` sigil variable implies a `Callable` type constraint for
  things that support to be called via the operator `()`,
  and it offers the same shortcuts for assignment which gives you a
  `Callable` and creates `Scalar` for the value:

## Scalarized

Along the way we learned that assignment to a `$`-sigiled variable gives you a
free `Scalar` container. This applies to both single values (e.g, a number, a
string, etc.) and compound values (e.g., a list of number, a list of strings,
etc.).

For a `$`-sigiled variable, the assignment of an entire list (or any
compound for that matter) is still a single thing, namely *a list*,
and it treats it as such. This is best demonstrated by comparing
a `List` **bound** to a `$`-sigiled variable (in which case no`Scalar`
is involved) and a `List` that is **assigned** to a `$`-sigiled 
variable (in which case an automatic `Scalar` container is created):

```raku
# Binding
my $list := (1, 2, 3);
say $list.raku;
say "Item: $_" for $list;

# OUTPUT:
# (1, 2, 3)
# Item: 1
# Item: 2
# Item: 3

# Assignment
my $list = (1, 2, 3);
say $list.raku;
say "Item: $_" for $list;

# OUTPUT:
# $(1, 2, 3)
# Item: 1 2 3
```

The `raku` method gave us an extra insight and showed the second `List` with a
`$` before it, to indicate it's containerized in a `Scalar`. More importantly,
when we iterated over our `List`s with the `for` loop, the second `List`
resulted in just a single iteration: the entire `List` as one item! The Scalar
lives up to its name.

Recall that `Array`s (and `Hashe`s) create `Scalar` containers for each of
their individual values. This means that if we nest things, even if we select
an individual list or hash stored inside the `Array` (or `Hash`) and try to
iterate over it, it'd be treated as just a single item:

```raku
my @things = (2, 4, 6), %(:France<Paris>, :Peru<Lima>);

say @things[0];           #=> $(2, 4, 6)
say @things[0].VAR.^name; #=> Scalar
say @things[1];           #=> ${:France("Paris"), :Peru("Lima")}
say @things[1].VAR.^name; #=> Scalar
```

This is a testament to the consistency of `Scalar` containers. Single items,
regardless of their structure, are still single items inside `Scalar`
containers. For example, this is the behaviour that applies when you try to
flatten an `Array`'s elements or pass them as an argument to a slurpy parameter:

```raku
my @things = (2, 4, 6), %(:France<Paris>, :Peru<Lima>);
say flat @things;

-> *@args { @args.say }(@things)
```

It's this behaviour that can drive Raku beginners up the wall,
especially those who come from auto-flattening languages, such as Perl.

## Binding vs Assignment

All the following might quite evident from the discussion thus far but 
it's still worthwhile to summarize it here.

Whenever you bind a right-hand side entity to a variable, the creation
of a container (`Scalar`, `Array`, etc.) is skipped and the entity is
bound directly to the variable, regardless of what the variable's
sigil might suggest. In a binding operation, the only thing a sigil
gives away about the variable is the type of values that can
be bound to it. For example, as mentioned earlier, the sigil `@` implies
the `Positional` role and only values of the types (`List`, `Array`,
`Range`, and `Buf`) implementing that role can be bound to an
`@`-sigiled variable.

```raku
# anything goes with $
my $anythinga         := 1;                # OK!
my $anythingb         := 1, 2;             # OK!
my $anythingc         := {A => 1, B => 2}; # OK!

# only positionals for @
my @only-positionala  := 1;                # Error
my @only-positionalb  := 1, 2;             # OK!
my @only-positionalc  := {A => 1, B => 2}; # Error

# only associatives for %
my %only-associativea := 1;                # Error
my %only-associativeb := 1, 2;             # Error
my %only-associativec := {A => 1, B => 2}; # OK!
```

On the other hand, an assigment prompts the compiler to create an automatic
container, which is dictated by the variable's sigil (e.g., `$` for
`Scalar`, `@` for `Array`, etc.), store whatever value in the container,
and ultimately bind the container to the variable.

Thus, the responsibility of a sigil during an assigment is twofold:

* restrict the types of values that can be bound. For the sigil `@`, this means
  only values that implement the `Positional` role can be bound to a
  `@`-sigiled variable.

* instruct the compiler to create the respective container for the sigil. For
  the sigil `$`, this means creating the `Scalar` container. And for the
  the `Array` container the sigil `@`, 
