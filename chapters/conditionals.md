# Conditionals

## If

Like all languages, Raku provides *conditionals*. The simplest form 
is the `if` statement:

```raku
my $number = 5;
if $number > 0 {
    say "$number is positive";
}
```

If the condition is true, the subsequent block of code is executed.
If not, nothing happens. 

The `if` statement has a *statement modifier* form:

```raku
my $number = 5;
say "$number is positive" if $number > 0;
```

A second form of the `if` statement is "alternative execution", in which
there are two possibilities and the condition determines which one runs.
Since the condition must be either true or false, exactly one of the
*branches* will run.

```raku
my $number = 5;
if $number > 0 {
    say "$number is positive"
}
else {
    say "$number isn't positive"
}
```

When there are more than two possibilities, multiple branches can be chained.
If there's an `else` clause, it has to be placed at the end, but there doesn't
have to be one.

```raku
my $number = 5;
if $number > 0 {
    say "$number is positive"
}
elsif $number < 0 {
    say "$number is negative"
}
else {
    say "$number is zero"
}
```

## With

The `if` conditional test for *truthiness* while `with` tests for
*defined-ness*. This is quite useful for conditions that evaluate to false,
and which evaluate to true when the routine `defined` is called on them.

```
# No output
say "Found an 'a'" if "abc".index('a');

# Output: Found an 'a'
say "Found an 'a'" if "abc".index('a').defined;

# Output: Found an 'a'
say "Found an 'a'" with "abc".index('a');
```

Similar to how `if` has the `else` for alternative, `with` has `orwith`.

```raku
my $str = "abc";

with $str.index('d') {
    say "Found an 'd' in $str"
}
orwith {
    say "Didn't find a 'd' in $str"
}
```

Another important thing about `with` is that, unlike `if`, it topicalizes
on its condition argument. This means that it makes its condition available as
the topic variable `$_` inside blocks.

As with `unless`, you may use `without` to check for undefinedness,
but you may not add an else clause:

## Given/when



