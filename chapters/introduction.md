# Introducing Raku

Raku is a gradually-typed, highly expressive and feature rich programming
language. Among some of its features are:

* Object-oriented programming including generics, roles and multiple dispatch
* Functional programming primitives, lazy and eager list evaluation, junctions,
  autothreading and hyperoperators (vector operators)
* Parallelism, concurrency, and asynchrony including multi-core support
* Definable grammars for pattern matching and generalized string processing
* Optional and gradual typing

As with any decent book introducing a programming language, we start with the
canonical Hello World example. Using your favorite editor, create a file
called `hello.raku` with the following contents:

```raku
# Our first Raku program
say "Hello, World!"
```

The first line is a comment. The simplest type of comments are introduced
with an octothorp (`#`) and they extend until the end of the line. To create
multiline comments, start with an octothorp and a backstick (`` #` ``) followed
by a pair of matching bracketing characters with the comment's contents inside
them:

```raku
#`{
This is a comment.
Adding a newline to make multiline.
You can also #`[nest multiline #`«comments inside» one another]
}
```

To run this program, execute `raku` followed by the filename in the command line:

```bash
$ raku hello.raku
Hello, World!
```

Running `raku -h` display the compiler's help message. For example, it
tells us that executing `raku -e` will allow us to execute one line of
a program:

```raku
$ raku -e 'say "Hello, World!"'
Hello, World!
```

If no filename is provided, you'll dropped in the REPL
(**R**ead-**E**valuate-**P**rint **L**oop) which reads your input, evaluates it,
prints the result (if any), and then waits for more input from you. This
indicated by the prompt `>`. Sometimes, it is easier, especially when you are
prototyping and don't have a concrete idea of the final result, to simply type
expressions directly at the REPL and see what happens. For example:

```raku
> say "Hello, World!"
Hello, World!
>
```

As shown above, the REPL reads the input, evaluates it, prints the result, and
waits for more input with `> `. Actually, we could've simply typed the
expression `"Hello, World!"` at the REPL, and we would have obtained the
same result as well:

```text
> "Hello, World!"
Hello, World!
> 2 * (42 + 3)
90
> 2 ** 3 == 8
True
>
```

This is because the REPL always evaluates the last read expression and display
its value. As shown above, the REPL can also be used as a handy calculator.

We are still at the REPL. To exit, either type `exit` or press `CTRL + D`
as instructed when you enter the REPL everytime to get back to the
operating-system command line:

```raku
$ raku
To exit type 'exit' or '^D'
> exit
$
```

The REPL is convenient for interactive testing of programs and program
fragments. However it is by no means necessary. It's customary to place
your executale Raku programs in files with the extension `.raku` which are then
executed with the command `raku`, as shown above. 
